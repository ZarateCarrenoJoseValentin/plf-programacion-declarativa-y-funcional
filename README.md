# <img src="images/logo.jpg" width="70" height="70"/> INSTITUTO TECNOLÓGICO DE OAXACA
## PROGRAMACION LOGICA Y FUNCIONAL - SCC1019 - ISB

### Programación declarativa y funcional
> Entregarás mapas conceptuales por cada uno de los siguientes temas:

### Mapa conceptual. Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

> El contenido del mapa conceptual que se presenta a continuación, radica como tema principal la "Programación declarativa", haciendo énfasis a las ideas principales que la conforman. 

```plantuml

@startmindmap
skinparam backgroundColor #D2C5A9
skinparam sequenceMessageAlign center
skinparam shadowing true
skinparam component {
FontName Arial
FontSize 17
}
<style>
mindmapDiagram{
    node{
        BackGroundColor  #87ABA6
    }
}
</style>

skinparam title {
FontName Arial 
FontSize 40
Fontcolor #fff
FontStyle bold
BackgroundColor #000
ArrowFontColor #000 
}
title " Programación Declarativa"

*[#43C790] Programación declarativa
**_ es\nun
*** Paradigma de programación 

****_ esta basado\nen el
***** Desarrollo de\nprogramas
******_ esta
******* especifica\no\ndeclara 
********_ un\nconjunto de
********* condiciones
********* proposiciones
********* afirmaciones
********* restricciones
********* ecuaciones 

****_ funciona a\nun
***** Nivel de abstracción\n muy alto
******_ maneja\nun
******* Margen de optimización\nmuy alto

**_ algunos\nejemplos de
*** Lenguajes de\nprogramación\ndeclarativa
****_ son

***** Lisp
******_ esta relacionado 
******* Programación Funcional

***** Haskell
******_ diseñado para
******* Programación Funcional
******_ utiliza 
******* Compiladores 
********_ estos\nson
********* Gofer
********* Guix 

***** Prolog
******_ implementa
******* Programación Lógica

***** Curry
******_  implementa
******* Programación\nlógico-funcional 
***** ML 
******_ diseñado\npara
******* Programación Funcional

**_ considera los\nprogramas como
*** Teorias
****_  y considera\nlos
***** Cálculos
******_ como
******* deducciones

****_ de una
***** Lógica formal
******_ se clasifican\nen
******* Lógica de\nprimer orden
******* Lógicas clasicas
******* Lógica modal\ny no clásica
******* Lógica algebraica 

**_ simplifica\nla
*** Escritura de\nprogramas paralelos
****_ indican\ncomo
***** Resolver un problema 
******_ mediante
******* sentencias
********_ trabajan\nen una
********* Forma descriptiva


**_ fuentes bibliografias 
*** Programación Funcional
****_ autor 
***** Jeroen Fokker
*** Lenguajes de Programación:\n Conceptos y constructores
****_ autor
***** Ravi Sethi

**_ se clasifican\nen
*** dos paradigmas
****_ estos\nson

***** Programación lógica

******_ basado en 
******* lógica de\nprimer orden 

******_ gira en\ntorno
******* concepto de\npredicado 
******* relación entre\nelementos

******_ algunas 
******* Características 
********_ son
********* Mecanismos de\ninferencia automática
********* Recursion como estructura\nde control básica
********* Vision lógica de\n la computación 

******_ ¿Qué estudia?
******* Lógica para el\nplanteamiento de\nproblemas
********_ tambien tiene
********* control sobre las reglas\nde inferencia

***** Programación Funcional

******_ se basa
******* concepto de función

******_ es un
******* corte matemático

******_ algunas 
******* Características
********_ son
********* Valores inmutables
********* Funciones matemáticas\npuras
**********_ pueden ser 
*********** sin estado\ninterno
*********** sin efectos\nlaterales
********* Uso profuso de la recursion
**********_ en la 
*********** definición de funciones
********* Uso  de listas como\nestructura de\ndatos
********* Funciones 
**********_ estas\npueden ser
*********** tipos de datos\nprimitivos 

**_ algunas
*** Ventajas
****_ son
***** Ser razonados matemáticamente
******_ permite
*******  uso de mecanismos\nmatemáticos
********_ ¿Qué hacen?
********* Optimizar el rendimiento\nde los programas

***** Menor tiempo de desarrollo
***** Liberación de asignaciones 
@endmindmap
```
**Fuente de información:**
1. [Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c66b1111f54718b4911)
2. [Programación Declarativa. Orientaciones y pautas para el estudio](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)
3. [Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)
 


### Mapa conceptual. Lenguaje de Programación Funcional (2015)
> El contenido del mapa conceptual que se presenta a continuación, hace énfasis en que es la "Programación Funcional", por lo tanto contiene definición, características y demás conceptos para la mejor compresión del lector.

```plantuml

@startmindmap
skinparam backgroundColor #D2C5A9
skinparam sequenceMessageAlign center
skinparam shadowing true
skinparam component {
FontName Arial
FontSize 17
}
<style>
mindmapDiagram{
    node{
        BackGroundColor  #2EC4C8
    }
}
</style>

skinparam title {
FontName Arial 
FontSize 30
Fontcolor #fff
FontStyle bold
BackgroundColor #000
ArrowFontColor #000 
}
title " Lenguaje de Programación Funcional"

*[#43C790] Lenguaje de programación\n Funcional

**_ utiliza\nel            
*** Paradigma de programación\n declarativa
****_ se bas\en el
***** Uso de funciones matemáticas
******_ ¿Qué se\npuede hacer?
******* Funciones de orden\n superior

****_ las
***** Funciones
******_ son
******* Ciudadanas de\nprimera clase
********_ sus 
********* Expresiones 
**********_ pueden ser
*********** Asignadas a variables


**_ Programación\nfuncional
*** Características
****_ son
***** Lenguaje expresivo
***** Matemáticamente elegante
***** Uso de recursividad
***** Aplicación de funciones
***** Vistos como elaboracion\n del cálculo lambda

**_ Lenguajes\n funcionales
*** Utilizados en aplicaciones
****_ pueden ser
***** Comerciales
***** Industrial

***_ tipos de\nlenguajes
**** Lisp
**** OCaml
**** Scala
**** Clojure
**** Erlang
**** Haskell

**_ algunas
*** características
****_ son
***** Relación entre\nuna entrada y salida
***** Considerada una\nfunción matemática
***** Crear programas con el\nenfoque de una función
***** Ausencia de efectos colaterales
***** Función mediante\nlos parámetros formales  
***** Las variables refieren\na una zona de memoria
******_ es el 
******* Lugar donde se\nalmacena un valor

**_ consecuencias de 
*** Ausencia de Variables\ny Asignación
****_ la

***** transparencia referencial

******_ se refiere 
******* Cualquier función 
********_ esta no
********* depende del orden de\nevaluación 
**********_ de sus 
*********** parámetros

******_ el valor
******* Cualquier función 
********_ depende
********* Valores de sus\n parámetros

******_ facilita el
******* Empleo de la\nprogramación funcional 

*** No existe estado
****_ por lo\ntanto
***** No existe concepto\nde localización
******_ estos son
******* Memoria
******* Variable

*** Ambiente de\nejecución

****_ este
***** Asocia valores\ncon nombres
******_ asi mismo
******* Locaciones\nde memoria  

****_ puede ser 
***** Semántica de\nalmacenamiento
***** Semántica de\napuntadores

*** Manipulación de funciones 
****_ son
***** Sin restricciones arbitrarias
******_ deben ser
******* Vistas como valores en sí

******_ pueden ser
******* Ejecutadas por\notras funciones
******* Parámetros de otras\nfunciones
@endmindmap
```
**Fuente de información:**
1. [Lenguaje de Programación Funcional](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)

### Estudiante ✒️
Zarate Carreño José Valentín